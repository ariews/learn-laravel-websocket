<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
        });
    }

    public function render($request, Throwable $e)
    {
        $e = $this->prepareException($this->mapException($e));

        return $this->prepareJsonResponse($request, $e);
    }

    /**
     * Convert the given exception to an array.
     */
    protected function convertExceptionToArray(Throwable $e): array
    {
        $code = $this->isHttpException($e) ? $e->getStatusCode() : 500;
        if (! $message = $e->getMessage()) {
            $message = Response::$statusTexts[$code];
        }

        if (config('app.debug')) {
            return [
                'code' => $code,
                'message' => $message,
                'exception' => get_class($e),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => collect($e->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all(),
            ];
        }

        return [
            'code' => $code,
            'message' => $message,
        ];
    }
}
